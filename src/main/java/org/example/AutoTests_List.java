package org.example;
import org.example.ProjectData.ProjectException;
import org.example.ProjectData.Resources.Autgorization.AuthorizationException;
import org.junit.jupiter.api.Test;
import jdk.jfr.Name;

import static org.example.ProjectData.ProjectOperation.ProjectOperation.authorizationBadLogin;
import static org.example.ProjectData.ProjectOperation.ProjectOperation.authorizationBadPassword;
import static org.example.ProjectData.TestStandGB_Tests.*;

public class AutoTests_List {

    @Test
    @Name("проверка авторизации (позитивный)")
    void testAuthorization() throws InterruptedException, AuthorizationException {authorizationTest();}

    @Test
    @Name("проверка авторизации (негативный: неверный логин)")
    void testAuthorizationBadLogin() throws InterruptedException, AuthorizationException {authorizationBadLoginTest();}

    @Test
    @Name("проверка авторизации (негативный: неверный пароль)")
    void testAuthorizationBadPassword() throws InterruptedException, AuthorizationException {authorizationBadPasswordTest();}


    @Name("Тест на спецсимволы")
    void testSpecialSymbol(){}

    @Test
    @Name("Тест Граничных значений: длина логина")
    void testLoginLength() throws AuthorizationException, InterruptedException {authorizationBorderTesting_lengthLogin();}

    @Name("Пустой логин")
    void testEmptyLogin(){}
    @Name("Пустой пароль")
    void testEmptyPassword(){}
//--------------ТЕСТИРОВАНИЕ СТРАНИЦЫ ПУБЛИКАЦИЙ
    //тест на верстку

    //тест на количество своих публикаций (нет своих публикаций, ожидаемое количество публикаций)

    //переключение режима публикаций свои-чужие

    @Test
    @Name("Тест количество моих отображаемых превью")
    void testCheckAmountMyPreview() throws AuthorizationException, InterruptedException, ProjectException {checkAmountMyPreviewTest();}

    @Test
    @Name("Тест количество отображаемых чужих превью")
    void testCheckAmountOtherPreview() throws AuthorizationException, InterruptedException, ProjectException {checkAmountOtherPreviewTest();}

    //TODO
    @Test
    @Name("Тест пагинация и количество постов")
    void testPagination() throws AuthorizationException, InterruptedException, ProjectException {paginationTest();}

    //тест на сортировку

    //тест на переходы по страницам

    @Test
    @Name("Тест создание публикации")
    void testCreatePost() throws AuthorizationException, InterruptedException {createPostTest();}

    @Test
    @Name("Тест удаление публикации")
    void testDeletePost() throws AuthorizationException, InterruptedException, ProjectException {deletePostTest();}



}
