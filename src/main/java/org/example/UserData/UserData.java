package org.example.UserData;

import org.example.ProjectData.Resources.Autgorization.AuthorizationException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import static org.example.ProjectData.Resources.Autgorization.AuthorizationException.*;
import static org.example.ProjectData.Resources.Autgorization.AuthorizationUI.userGreeting;
import static org.example.ProjectData.Resources.PageToolBar.toolbarBlockLocator;
import static org.example.ProjectData.Resources.PageToolBar.toolbarUserGreetingLocator;
import static org.example.WebDriver.ProjectWebDriver.getChromeDriver;

public class UserData {
    private String login;
    private String password;
    UserData(String login, String password){
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
    public static UserData currentUser;
    public static final UserData user = new UserData("EvgenyT","10f3c68a51");
    public static final UserData userNormal = user;

    public static final UserData badUser = new UserData("notUser","badPassword");
    public static final UserData badUserLoginTooShort = new UserData("Lo","badPassword");
    public static final UserData badUserLoginTooLong = new UserData("Looooooooooooooooogin","badPassword"); //21 символ
    public static final UserData userMinimalLogin = new UserData("Lgn","41d0bc6a95");
    public static final UserData userMaximalLogin = new UserData("Loooooooooooooooogin","827e10e596");  //20 символов




    public static void checkAuthorization() throws InterruptedException, AuthorizationException {
        System.out.println("Проверка авторизации");
        WebElement headerToolBar;
        WebElement headerUserGreeting;
        WebElement errorBlock;
        WebElement errorBlockCode;
        WebElement errorBlockText;

        //проверяем наличие тулбара в котором должно отобразиться приветствие пользователю (он есть почти всегда,
        // даже если не отображается)
        try {
            headerToolBar = getChromeDriver().findElement(By.xpath("//nav"));
        }catch (WebDriverException wde1){
            System.out.println("ОШИБКА АВТОРИЗАЦИИ: вход не выполнен");
            headerToolBar = getChromeDriver().findElement(By.xpath("//nav"));
        }

        //проверяем наличие приветствия пользователю
        Thread.sleep(2000);
        try{
            headerUserGreeting = getChromeDriver().findElement(By.xpath("//nav//a[contains(text(), \"Hello\")]"));
            assert headerUserGreeting.getText().intern() == (userGreeting + currentUser.getLogin()).intern() : authorizationUserDefinitionException;
        }catch (WebDriverException wde1){
            System.out.println("ОШИБКА АВТОРИЗАЦИИ: приветствие пользователя не обнаружено!");
            errorBlock = getChromeDriver().findElement(By.xpath("//div[contains(@class, \"error-block\")]"));
            errorBlockCode = getChromeDriver().findElement(By.xpath("//div[contains(@class, \"error-block\")]//h2"));                      //   код ошибки
            errorBlockText = getChromeDriver().findElement(By.xpath("//div[contains(@class, \"error-block\")]//h2/following::p[1]"));      //    расшифровка ошибки
            assert errorBlockCode.getText().intern() != "401" : authorizationInvalidCredentialsException; ;
        }
        System.out.println("Пользователь успешно авторизован");
    }

    public static void checkFailedAuthorization(AuthorizationException expectedError) throws InterruptedException {
        WebElement errorBlock;
        WebElement errorBlockCode;
        WebElement errorBlockText;

        Thread.sleep(1000);
        errorBlock = getChromeDriver().findElement(By.xpath("//div[contains(@class, \"error-block\")]"));
        errorBlockCode = getChromeDriver().findElement(By.xpath("//div[contains(@class, \"error-block\")]//h2"));                      //   код ошибки
        errorBlockText = getChromeDriver().findElement(By.xpath("//div[contains(@class, \"error-block\")]//h2/following::p[1]"));

        //System.out.println("expectedError.getErrorCode() = " + expectedError.getErrorCode());
        //System.out.println("errorBlockCode.getText() = " + errorBlockCode.getText());

        //assert "1" == "1" : "1 и 1";
        assert expectedError.getErrorCode().intern() == errorBlockCode.getText().intern() : authorization401ExpectedException;

        System.out.println("Пользователь не был авторизован: " + expectedError.getMessage());


    }




    public static void checkLogOut() throws InterruptedException {
        Thread.sleep(3000);
        System.out.println("Проверка выхода пользователя");
        WebElement toolbarBlock;
        WebElement toolUserGreeting;

        //Проверка наличия тулбара - может быть скрыт при разлогине (явно в требованиях не указано)
        try {
            toolbarBlock = getChromeDriver().findElement(By.xpath(toolbarBlockLocator));
//            System.out.println("(!) Внимание: тулбар пользователя не скрыт (!)");
        }catch (WebDriverException wde){
            System.out.println("Пользователь успешно вышел из системы (скрыт тулбар пользователя)");
            return;
        }
        //если тулбар был найден, то возможно пользователь все же разлогинен
        try {
            toolUserGreeting = getChromeDriver().findElement(By.xpath(toolbarUserGreetingLocator));
            System.out.println("(!) Внимание: обнаружено приветствие пользователя (!)");
        }catch (WebDriverException wde){
            System.out.println("Пользователь успешно вышел из системы (приветствие пользователя не обнаружено)");
            return;
        }
        //если приветствие пользователя обнаружено, то возможно пользователь - гость
        //TODO:
   //     System.out.println("toolUserGreeting.getText() = " + toolUserGreeting.getText());
   //     System.out.println("userGreeting + currentUser.getLogin() = " + (userGreeting + currentUser.getLogin()));
        assert toolUserGreeting.getText().intern() != (userGreeting + currentUser.getLogin()).intern() : "Отображается вход пользователя"   ;
        System.out.println("Пользователь успешно вышел из системы");
    }
}
