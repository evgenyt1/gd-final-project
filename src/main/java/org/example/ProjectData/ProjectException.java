package org.example.ProjectData;

import static org.example.ProjectData.Resources.Post.getAmountMyPreview;

public class ProjectException extends Exception{

    ProjectException(String message){
        super(message);
    }

    public static ProjectException postNotDeleted = new ProjectException("Публикация не была удалена (отображается в списке публикаций пользователя)");
    public static ProjectException targetPageUrlException = new ProjectException("Открытый ресурс не соответствует ожидаемому");
    public static ProjectException displayPostValueException = new ProjectException("Количество отображаемых превью = " + getAmountMyPreview() + "(не соответствует ожидаемому)");

}
