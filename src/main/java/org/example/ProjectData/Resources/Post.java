package org.example.ProjectData.Resources;

import org.example.ProjectData.ProjectException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import java.util.Date;
import java.util.List;

import static org.example.Other.DataRandomizer.dateFormatting;
import static org.example.Other.DataRandomizer.dateTodayJava;
import static org.example.ProjectData.ProjectException.displayPostValueException;
import static org.example.ProjectData.ProjectException.postNotDeleted;
import static org.example.ProjectData.Resources.PagePostView.locatorContent;
import static org.example.ProjectData.Resources.PagePostView.locatorTitle;
import static org.example.WebDriver.ProjectWebDriver.getChromeDriver;

public class Post {

    public static class PostType{
        Boolean switchDisplayTypeValue;
        PostType(Boolean switchDisplayTypeValue){
            this.switchDisplayTypeValue = switchDisplayTypeValue;
        }
        public static PostType myPost = new PostType(false);
        public static PostType otherPost = new PostType(true);
    }

    String title;
    String description;
    String content;

    public Post(String title, String description, String content){
        this.title = title + dateFormatting(dateTodayJava);
        this.description = description;
        this.content = content;
    }
    public Post(String title){
        this.title = title + dateFormatting(dateTodayJava);
    }


    public static Post currentPost;

    public static void checkDeleting(Post deletedPost) throws ProjectException, InterruptedException {
        Thread.sleep(2000);
        try{
            WebElement deletedPostItem = getChromeDriver().findElement(By.xpath("//tr/td[text()=\"" +deletedPost.title+ "\"])"));
        }catch (WebDriverException wde){
            System.out.println("Пост успешно удален (не значится в списке публикаций пользователя)");
            return;
        }
        throw postNotDeleted;
    }
    public static void  checkAmountMyPreview(int expectedAmountPreviewPost){
        String locatorPost = "//a[contains(@class, \"post\")]";
        List<WebElement> postOnPage = getChromeDriver().findElements(By.xpath(locatorPost));
        assert postOnPage.size() == expectedAmountPreviewPost: displayPostValueException ;
    }
    public static int  getAmountMyPreview(){
        String locatorPost = "//a[contains(@class, \"post\")]";
        List<WebElement> postOnPage = getChromeDriver().findElements(By.xpath(locatorPost));
        int postOnPageAmount = postOnPage.size();
        return postOnPageAmount;
    }



}
