package org.example.ProjectData.Resources;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.example.ProjectData.ProjectOperation.ProjectOperation.click;
import static org.example.WebDriver.ProjectWebDriver.getChromeDriver;

public class PageToolBar {
    public static class Toolbar{
        String url;
        String locatorToTransfer;

        public Toolbar(String url, String locatorToTransfer) {
            this.url = url;
            this.locatorToTransfer = locatorToTransfer;
        }
    }
    public static class ResourcesToolbar extends Toolbar{

        public static final String toolbarDropDownProfileLocator = "";
        ResourcesToolbar(String url, String locatorToTransfer){
            super(url,locatorToTransfer);
        }
        public static final ResourcesToolbar profile = new ResourcesToolbar("https://test-stand.gb.ru/user/profile", "//button[@id=\"create-btn\"]");
    }
    public static class ResourcesToolbarDropDown extends Toolbar {
        public static final String toolbarDropDownProfileLocator = "";

        ResourcesToolbarDropDown(String url, String locatorToTransfer) {
            super(url,locatorToTransfer);
        }

        public static ResourcesToolbarDropDown profile = new ResourcesToolbarDropDown("https://test-stand.gb.ru/user/profile","//span[text()=\"Profile\"]//ancestor::li[@role]");

    }

        public static final String toolbarBlockLocator = "//nav";
    public static final String toolbarUserGreetingLocator = "//nav//a[contains(text(), \"Hello\")]";
    public static final String toolbarDropDownLogoutLocator = "//span[text()=\"Logout\"]//ancestor::li[@role]";
    public static final String toolbarDropDownProfileLocator = "//span[text()=\"Profile\"]//ancestor::li[@role]";
    public static final String toolbarHomeLocator = "//span[text()=\"Home\"]//ancestor::a";

    public static WebElement toolbarBlock = getChromeDriver().findElement(By.xpath("//nav"));
    public static WebElement toolbarUserGreeting = getChromeDriver().findElement(By.xpath("//nav//a[contains(text(), \"Hello\")]"));
    public static WebElement toolbarDropDownLogout = getChromeDriver().findElement(By.xpath("//span[text()=\"Logout\"]//ancestor::li[@role]"));

    public static void navigationToolbar(Toolbar toolbar){
        if (toolbar instanceof ResourcesToolbar){
            click(toolbar.locatorToTransfer);
        }
        if (toolbar instanceof ResourcesToolbarDropDown){
            click(toolbarUserGreetingLocator);
            click(toolbar.locatorToTransfer);
        }
    }

}
