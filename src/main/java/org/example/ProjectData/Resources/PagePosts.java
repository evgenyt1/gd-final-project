package org.example.ProjectData.Resources;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.example.ProjectData.Resources.Post.currentPost;
import static org.example.WebDriver.ProjectWebDriver.getChromeDriver;

public class PagePosts {
    public static final String locatorToPageTransfer = "";


    public static void checkingNewPostPreview() throws InterruptedException {
        Thread.sleep(2000);
        System.out.print("Проверка нового поста: ");
        checkingPostPreview(1);
    }

    public static void checkingPostPreview(int numberPostOnBlogPreview){
        System.out.println("Проверка поста " + numberPostOnBlogPreview);

        String locatorPost = "//a[contains(@class, \"post\")]["+ numberPostOnBlogPreview +"]";
        String locatorPostTitle = locatorPost + "/h2";
        String locatorPostDescription = locatorPost + "/div";

        WebElement postTitle = getChromeDriver().findElement(By.xpath(locatorPostTitle));
        assert postTitle.getText().intern() == currentPost.title.intern() : "Заголовок поста сохранен неверно!";


        WebElement postDescription = getChromeDriver().findElement(By.xpath(locatorPostDescription));
        assert postDescription.getText().intern() == currentPost.description.intern() : "Подзаголовок поста сохранен неверно!";
    }

    public static void checkPostsDisplayType(Boolean otherPosts){
        //button[@role="switch"][@aria-checked="true"]
        String locatorChecker = "//span[text()=\"Not my Posts\"]/ancestor::label/preceding-sibling::button";
        WebElement checkerPostsDisplayType = getChromeDriver().findElement(By.xpath(locatorChecker));

        String pagePostDisplay_stringValue = checkerPostsDisplayType.getAttribute("aria-checked");
        Boolean pagePostDisplayTypeOthers = Boolean.parseBoolean(pagePostDisplay_stringValue);

        assert pagePostDisplayTypeOthers == otherPosts: "Режим отображения постов не соответствует ожидаемому!";
    }

    public static void changePostsDisplayType(Post.PostType newDisplayType){
        //true - чужие посты
        //false - свои посты
        //button[@role="switch"][@aria-checked="true"]
        String locatorChecker = "//span[text()=\"Not my Posts\"]/ancestor::label/preceding-sibling::button";
        WebElement checkerPostsDisplayType = getChromeDriver().findElement(By.xpath(locatorChecker));

        String pagePostDisplay_stringValue = checkerPostsDisplayType.getAttribute("aria-checked");
        Boolean pagePostDisplayTypeOthers = Boolean.parseBoolean(pagePostDisplay_stringValue);

        if(newDisplayType.switchDisplayTypeValue == pagePostDisplayTypeOthers){
            //ничего не делать
        }else {
            checkerPostsDisplayType.click();
        }
    }

}
