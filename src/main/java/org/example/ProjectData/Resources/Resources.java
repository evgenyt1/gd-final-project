package org.example.ProjectData.Resources;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.example.ProjectData.ProjectException.targetPageUrlException;
import static org.example.ProjectData.Resources.PageToolBar.toolbarHomeLocator;
import static org.example.WebDriver.ProjectWebDriver.getChromeDriver;

public class Resources {
    String ulr;
    String locatorToTransfer;
    public static final String project = "https://test-stand.gb.ru/login";

    Resources(String url, String locatorToTransfer){
        this.ulr = url;
        this.locatorToTransfer = locatorToTransfer;
    }

    public static final Resources createPost = new Resources("https://test-stand.gb.ru/posts/create", "//button[@id=\"create-btn\"]");
    public static final Resources blogMyPosts = new Resources("https://test-stand.gb.ru/", toolbarHomeLocator);

    public static void navigationTo(Resources resources){
        WebElement linkToResource = getChromeDriver().findElement(By.xpath(resources.locatorToTransfer));
        linkToResource.click();
    }

    public static void checkResources(Resources expectedResources){
        String currentUrl = getChromeDriver().getCurrentUrl();
        assert currentUrl.intern() == expectedResources.ulr.intern() : targetPageUrlException;
    }
}
