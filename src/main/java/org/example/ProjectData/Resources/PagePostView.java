package org.example.ProjectData.Resources;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.example.ProjectData.ProjectOperation.ProjectOperation.click;
import static org.example.ProjectData.Resources.Post.currentPost;
import static org.example.WebDriver.ProjectWebDriver.getChromeDriver;

public class PagePostView {
    static final String locatorTitle = "//h1";
    static final String locatorContent = "//div[contains(@class, \"content\")][not(@id)]";

    static final String locatorDeleteButton = "//button[text()=\"delete\"]";





    public  static void checkingPostView(Post postForChecking) throws InterruptedException {
        System.out.println("Проверка содержания поста на форме просмотра");
        Thread.sleep(2000);

        //проверка title
        WebElement title = getChromeDriver().findElement(By.xpath(locatorTitle));
        assert title.getText().intern() == postForChecking.title.intern() : "Заголовок поста сохранен неверно!";

        //проверка content
        WebElement content = getChromeDriver().findElement(By.xpath(locatorContent));
        assert content.getText().intern() == postForChecking.content.intern(): "Содержание поста сохранено неверно!";
    }

    public static void viewPost(int postNumber){
        String locatorPost = "//a[contains(@class, \"post\")]["+ postNumber +"]";
        String locatorPostTitle = locatorPost + "/h2";
        WebElement postTitle = getChromeDriver().findElement(By.xpath(locatorPostTitle));
        String postTitleText = postTitle.getText();

        currentPost = new Post(postTitleText);
        currentPost.title = postTitleText;

        postTitle.click();
    }

    public static void checkPostTitle() throws InterruptedException {
        System.out.println("Сравнение заголовка открытого поста");
        Thread.sleep(2000);

        WebElement title = getChromeDriver().findElement(By.xpath(locatorTitle));
        System.out.println("title.getText() = " + title.getText());
        System.out.println("currentPost.title = " + currentPost.title);
        
        assert title.getText().intern() == currentPost.title.intern() : "Заголовок открытого поста отличается!";
    }

    public static void deleteThisPost(){
        click(locatorDeleteButton);
    }

}
