package org.example.ProjectData.Resources;

import org.openqa.selenium.WebElement;

import static org.example.ProjectData.ProjectOperation.ProjectOperation.click;
import static org.example.ProjectData.ProjectOperation.ProjectOperation.inputTextField;
import static org.example.ProjectData.Resources.Post.currentPost;

public class PageCreatePost {
    public static final String locatorToPageTransfer = "//button[@id=\"create-btn\"]";
    static final String locatorInputTitle = "//span[text()=\"Title\"]/following::input[1]";
    static final String locatorInputDescription = "//span[text()=\"Description\"]/following::textarea[1]";
    static final String locatorInputContent = "//span[text()=\"Content\"]/following::textarea[1]";
    static final String locatorSavingButton = "//span[text()=\"Save\"]";


    public static void fillingCreatePostForm(Post postData) throws InterruptedException {
        System.out.println("Заполнение формы поста");
        Thread.sleep(2000);
        //currentPost = new Post(title,description,content);
        //title
        click(locatorInputTitle);
        inputTextField(locatorInputTitle,postData.title);
        //description
        click(locatorInputDescription);
        inputTextField(locatorInputDescription,postData.description);
        //content
        click(locatorInputContent);
        inputTextField(locatorInputContent,postData.content);
    }

    public static void savingCreatePostForm() {
        click(locatorSavingButton);
    }

    }
