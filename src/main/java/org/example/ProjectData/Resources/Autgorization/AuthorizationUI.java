package org.example.ProjectData.Resources.Autgorization;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static org.example.WebDriver.ProjectWebDriver.getChromeDriver;

public class AuthorizationUI {
    public static final String userGreeting = "Hello, ";
    public static final String locatorLoginInput = "//span[text()=\"Username\"]/following-sibling::input";
    public static final String locatorPasswordInput = "//span[text()=\"Password\"]/following-sibling::input";
    public static final String locatorLoginButton = "//span[text()=\"Login\"]//ancestor::button";

    public static final WebElement loginInput = getChromeDriver().findElement(By.xpath("//span[text()=\"Username\"]/following-sibling::input"));
    public static final WebElement passwordInput = getChromeDriver().findElement(By.xpath("//span[text()=\"Password\"]/following-sibling::input"));
    public static final WebElement loginButton = getChromeDriver().findElement(By.xpath("//span[text()=\"Login\"]//ancestor::button"));

    public static void authorizationUiRefresh(){

    }

}
