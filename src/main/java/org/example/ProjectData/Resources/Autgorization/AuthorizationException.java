package org.example.ProjectData.Resources.Autgorization;

public class AuthorizationException extends Exception{
    String errorCode;
    public AuthorizationException(String errorCode, String message){
        super(message);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public static final AuthorizationException authorizationToolBarAvailabilityException = new AuthorizationException("","Пользователь не прошел авторизацию авторизован");

    public static final AuthorizationException authorizationInvalidCredentialsException = new AuthorizationException("401","Пользователь не прошел авторизацию: 401 Invalid Credentials");
    public static final AuthorizationException authorization401ExpectedException = new AuthorizationException("401","Ошибка 401 не была выявлена");

    public static final AuthorizationException authorizationUserDefinitionException = new AuthorizationException("","Пользователь или имя пользователя определены неверно!");


    public static final AuthorizationException error401 = authorizationInvalidCredentialsException;

}
