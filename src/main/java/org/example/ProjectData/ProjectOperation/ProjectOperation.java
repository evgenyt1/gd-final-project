package org.example.ProjectData.ProjectOperation;

import org.example.ProjectData.Resources.Autgorization.AuthorizationException;
import org.example.ProjectData.Resources.PageToolBar;
import org.example.ProjectData.Resources.Resources;
import org.example.UserData.UserData;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import static org.example.ProjectData.Resources.Autgorization.AuthorizationException.authorization401ExpectedException;
import static org.example.ProjectData.Resources.Autgorization.AuthorizationException.error401;
import static org.example.ProjectData.Resources.Autgorization.AuthorizationUI.*;

import static org.example.ProjectData.Resources.PageToolBar.toolbarDropDownLogoutLocator;
import static org.example.ProjectData.Resources.PageToolBar.toolbarUserGreetingLocator;
import static org.example.UserData.UserData.*;
import static org.example.WebDriver.ProjectWebDriver.getChromeDriver;

public class ProjectOperation {
    public static void authorization(UserData user) throws InterruptedException, AuthorizationException {
        System.out.println("Процесс: авторизация пользователя "+user.getLogin());
        Thread.sleep(2000);
        UserData.currentUser = user;
        WebElement loginInput = getChromeDriver().findElement(By.xpath(locatorLoginInput));
        loginInput.sendKeys(user.getLogin());
        WebElement passwordInput = getChromeDriver().findElement(By.xpath(locatorPasswordInput));
        passwordInput.sendKeys(user.getPassword());
        WebElement loginButton = getChromeDriver().findElement(By.xpath(locatorLoginButton));
        loginButton.click();
        checkAuthorization();
    }
    public static void authorizationBadLogin(UserData badUser) throws InterruptedException, AuthorizationException {
        System.out.println("Процесс: попытка авторизации пользователя с неправильным логином");

        Thread.sleep(2000);
        loginInput.sendKeys(badUser.getLogin());
        passwordInput.sendKeys(userNormal.getPassword());
        loginButton.click();
        checkFailedAuthorization(error401);
    }
    public static void authorizationBadPassword(UserData badUser) throws InterruptedException, AuthorizationException {
        System.out.println("Процесс: попытка авторизации пользователя с неправильным паролем");


        Thread.sleep(2000);
        loginInput.sendKeys(userNormal.getLogin());
        passwordInput.sendKeys(badUser.getPassword());
        loginButton.click();
        checkFailedAuthorization(error401);

    }

    public static void logOut() throws InterruptedException {
        System.out.println("Процесс: выход пользователя из системы");

        Thread.sleep(1000);
        WebElement toolbarUserGreeting = getChromeDriver().findElement(By.xpath(toolbarUserGreetingLocator));
        toolbarUserGreeting.click();
        Thread.sleep(1000);
        WebElement toolbarDropDownLogout = getChromeDriver().findElement(By.xpath(toolbarDropDownLogoutLocator));
        toolbarDropDownLogout.click();

        checkLogOut();
    }

    public static void clearTextField_elementText(String locator){
       // System.out.println("Очистка поля");

        WebElement clearingField = getChromeDriver().findElement(By.xpath(locator));

        String textInField = clearingField.getText();

      //  System.out.println("значение в поле "+textInField);

        int amountSymbol = textInField.length();
        for(int i = 0; i < amountSymbol; i++){
            clearingField.sendKeys(Keys.BACK_SPACE);
        }
        WebElement fieldAfterClear = getChromeDriver().findElement(By.xpath(locator));
        assert fieldAfterClear.getText().length() == 0 : "Поле не очищено!";
    }

    public static void clearTextField_TextValue(String locator){
       // System.out.println("Очистка поля");

        WebElement clearingField = getChromeDriver().findElement(By.xpath(locator));

        String textInFieldValue = clearingField.getAttribute("value");
     //   System.out.println("значение в поле "+textInFieldValue);
        int amountSymbol = textInFieldValue.length();
        for(int i = 0; i < amountSymbol; i++){
            clearingField.sendKeys(Keys.BACK_SPACE);
        }
        WebElement fieldAfterClear = getChromeDriver().findElement(By.xpath(locator));
        assert fieldAfterClear.getText().length() == 0 : "Поле не очищено!";
    }

    public static void inputTextField(String locator, String text){
        WebElement fieldInput = getChromeDriver().findElement(By.xpath(locator));
        fieldInput.sendKeys(text);
    }

    public static void click(String locator){
        WebElement linkToResource = getChromeDriver().findElement(By.xpath(locator));
        linkToResource.click();
    }

}

