package org.example.ProjectData;

import org.example.ProjectData.Resources.Autgorization.AuthorizationException;
import org.example.ProjectData.Resources.Post;
import org.example.UserData.UserData;
import org.openqa.selenium.WebElement;

import static org.example.ProjectData.ProjectOperation.ProjectOperation.*;
import static org.example.ProjectData.Resources.Autgorization.AuthorizationUI.locatorLoginInput;
import static org.example.ProjectData.Resources.Autgorization.AuthorizationUI.locatorPasswordInput;
import static org.example.ProjectData.Resources.PageCreatePost.fillingCreatePostForm;
import static org.example.ProjectData.Resources.PageCreatePost.savingCreatePostForm;

import static org.example.ProjectData.Resources.PagePostView.*;

import static org.example.ProjectData.Resources.PagePosts.*;
import static org.example.ProjectData.Resources.PageToolBar.ResourcesToolbarDropDown.profile;
import static org.example.ProjectData.Resources.PageToolBar.navigationToolbar;
import static org.example.ProjectData.Resources.Post.*;
import static org.example.ProjectData.Resources.Post.PostType.otherPost;
import static org.example.ProjectData.Resources.Resources.*;
import static org.example.UserData.UserData.*;
import static org.example.WebDriver.ProjectWebDriver.webDriverStart;

public class TestStandGB_Tests {

    static public void authorizationTest() throws InterruptedException, AuthorizationException {
        webDriverStart(project);
        authorization(user);
        //ожидание
        Thread.sleep(2000);
        //разлогин
        logOut();
    }
    static public void authorizationTest(UserData user) throws InterruptedException, AuthorizationException {
        authorization(user);
        //ожидание
        Thread.sleep(2000);
        //разлогин
        logOut();
    }

    static public void authorizationBadLoginTest() throws InterruptedException, AuthorizationException {
        webDriverStart(project);
        authorizationBadLogin(user);
    }

    static public void authorizationBadPasswordTest() throws InterruptedException, AuthorizationException {
        webDriverStart(project);
        authorizationBadPassword(user);
    }

    static public void authorizationBorderTesting_lengthLogin() throws AuthorizationException, InterruptedException {
        webDriverStart(project);
        System.out.println("Начат тест граничных значений длины логина");

        //авторизация по слишком короткому логину
        authorizationBadLogin(badUserLoginTooShort);

        //авторизация по слишком длинному логину
        //очистка полей логин и пароль
        clearTextField_TextValue(locatorLoginInput);
        clearTextField_TextValue(locatorPasswordInput);
        //попытка авторизации
        authorizationBadLogin(badUserLoginTooLong);

        //авторизация по логину минимальной длины
        clearTextField_TextValue(locatorLoginInput);
        clearTextField_TextValue(locatorPasswordInput);
        authorizationTest(userMinimalLogin);

        //авторизация по логину максимальной длины
        authorizationTest(userMaximalLogin);
    }

    static public void createPostTest() throws AuthorizationException, InterruptedException {
        //создание логического объекта Пост
        currentPost = new Post("autoTest","Description","some content");

        //запуск вебдрайвера
        webDriverStart(project);

        //авторизация
        authorization(user);

        //переход на форму создания поста
        navigationTo(createPost);

        //заполнение формы
        fillingCreatePostForm(currentPost);

        //сохранение
        savingCreatePostForm();

        //проверка поста
        checkingPostView(currentPost);

        //проверка поста в ленте публикаций
        navigationTo(blogMyPosts);
        checkingNewPostPreview();
    }

    static public void deletePostTest() throws AuthorizationException, InterruptedException, ProjectException {
        //запуск вебдрайвера
        webDriverStart(project);

        //авторизация
        authorization(user);

        //открыть первый пост, запомнив его название
        viewPost(1);

        //проверить название после открытия
        checkPostTitle();

        //нажать удалить
        deleteThisPost();

        //проверить удаление
        navigationToolbar(profile);
        checkDeleting(currentPost);

    }
    static public void paginationTest() throws AuthorizationException, InterruptedException, ProjectException {
        //запуск вебдрайвера
        webDriverStart(project);

        //авторизация
        authorization(user);

        //проверка, что это страница ленты постов

        //проверка, что режим отображения = свои посты

        //проверка количества постов
    }

    static public void checkAmountMyPreviewTest() throws AuthorizationException, InterruptedException {
        //запуск вебдрайвера
        webDriverStart(project);

        //авторизация
        authorization(user);

        //проверка, что это страница ленты постов
        checkResources(blogMyPosts);

        //проверка, что режим отображения = свои посты
        checkPostsDisplayType(false);

        //проверка количества постов
        checkAmountMyPreview(10);
    }

    static public void checkAmountOtherPreviewTest() throws AuthorizationException, InterruptedException {

        //запуск вебдрайвера
        webDriverStart(project);

        //авторизация
        authorization(user);

        //проверка, что это страница ленты постов
        checkResources(blogMyPosts);

        //проверка, что режим отображения = свои посты
        checkPostsDisplayType(false);

        //переключение режима постов
        changePostsDisplayType(otherPost);

        //проверка, что режим отображения = чужие посты
        checkPostsDisplayType(true);

        //проверка количества постов
        checkAmountMyPreview(4);

    }


}
