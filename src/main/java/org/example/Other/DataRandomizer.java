package org.example.Other;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataRandomizer {
    public final static Date dateTodayJava = new Date();
    public static String dateFormatting(Date date){
        SimpleDateFormat dayDateFormat = new SimpleDateFormat("dd");
        SimpleDateFormat monthDateFormat = new SimpleDateFormat("MM");
        SimpleDateFormat hourDateFormat = new SimpleDateFormat("HH");
        SimpleDateFormat minuteDateFormat = new SimpleDateFormat("mm");

        String formattedDateMONTH = monthDateFormat.format(date);
        String formattedDateDAY = dayDateFormat.format(date);
        String formattedDateHOUR = hourDateFormat.format(date);
        String formattedDateMINUTE = minuteDateFormat.format(date);

        String formattedDate = formattedDateDAY+"."+formattedDateMONTH+" "+formattedDateHOUR+":"+formattedDateMINUTE;

        return formattedDate;
    }
}
